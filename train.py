"""
Given the model parameters, an RNN model is intantiated and the tracks in the training data are fed into model for training.
"""
import numpy as np
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import sys
import math
import time
from net import Model
from utils import timeSince

def train(label_tensor, track_tensor):
    """
    Given the track tensor containing the hits and corresponding label (e or muon), the function fits the data to the model by training given amount of epochs

    Args:
    label_tensor: Pytorch tensor containing 0 or 1 for muon/electron
    trakc_tenor: Pytorch tensor that contains a list of hits in a given track

    Output:
    output = The output of RNN model, given by a sequence of probabilities for each cell in RNN (the last one is optimised/used for assesing the probability of being an electron)
    loss = The loss value correspoinding to given track calculated by loss function

    """
    hidden = model.init_hidden()
    optimizer.zero_grad()

    for i in range(track_tensor.size()[0]):
        output, hidden = model(track_tensor[i], hidden)
        
    loss = criterion(output, label_tensor)
    loss.backward()
    optimizer.step()
    
    return output, loss.item()


if __name__ == '__main__':
    # Instantiate RNN model
    input_size = 6
    hidden_size = 50
    num_layers = 1
    batch_size = 1
    num_classes = 1
    dropout = 0
    model = Model(input_size,hidden_size,num_layers,batch_size,num_classes,dropout)
    print(model)

    # Set loss and optimizer function
    # CrossEntropyLoss = LogSoftmax + NLLLoss
    criterion = nn.BCELoss()
    optimizer = torch.optim.Adagrad(model.parameters(), lr=0.1)


    n_iters = len(training_data)
    print_label = False
    print_param = False
    all_losses = []
    plot_loss = True
    
    print_every = 100
    plot_every = 100
    numEpochs = 1

    for epoch in range(numEpochs):
        print("Epoch: ", epoch + 1)
        # Keep track of losses for plotting
        current_loss = 0
        
        start = time.time()
        counter = 0

        for track, label in training_data:
            counter += 1
            sys.stdout.write("progress: %d%%   \r" % (float(counter)*100./(len(training_data))) )
            sys.stdout.flush()
            track_tensor = torch.autograd.Variable(torch.FloatTensor(track))
            label_tensor = torch.FloatTensor([[[label]]])

            output, loss = train(label_tensor, track_tensor)
            current_loss += loss

            # Print iter number, loss, name and guess
            if(print_label == True):
                if counter % print_every == 0:
                    print('%d %d%% (%s) True label: %d LSTM output: %f eProbHT: %f loss: %.4f ' % (counter, counter / n_iters * 100, timeSince(start), label, output, train_eProbHT[counter-1], loss))
                    if(print_param == True):
                        for name, param in model.named_parameters():
                            if param.requires_grad:
                                print(name, param.data)
                                print(name, "grad: ", param.grad)
            if(plot_loss == True):
                if counter % plot_every == 0:
                    print(counter, "-", "loss: ", current_loss / plot_every)
                    all_losses.append(current_loss / plot_every)
                    current_loss = 0
                    
        print("Training finished for epoch: ", epoch + 1)
        print("Time for training: ",timeSince(start))



