"""
Evaluates the trained model on test data and reports ROC scores
"""
import numpy as np
import time
import torch
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, roc_auc_score
from utils import plotRocCurve

def evaluate(track_tensor):
	"""
	For a given test track hits, the function evaluates the probability of the track being from an electron or NIPs

	Args:
	track_tensor: Pytorch tensor containin the hits of a given test track

	Output:
	output: The probability of the track being electron
	"""
	hidden = model.init_hidden()

	for i in range(track_tensor.size()[0]):
		output, hidden = model(track_tensor[i], hidden)

	return output

def predict(test_data):
	"""
	Given a test data, the function evaluates all the tracks and outputs a list of probabilities being an electron

	Args:
	test_data: The test data given by make_data module, that contains a nested list of tracks

	Output:
	test_score: A list of probability scores returned by testing each tracks in test data

	"""
	test_score = []
	with torch.no_grad():
		counter = 0
		for track in test_data:
			counter += 1
			sys.stdout.write("progress: %d%%   \r" % (float(counter)*100./(len(test_data))) )
			sys.stdout.flush()
			track_tensor = torch.autograd.Variable(torch.FloatTensor(track))
			output = evaluate(track_tensor)
			test_score.append(output)
	return test_score

if __name__ == '__main__':
	y_scores = predict(X_test)
	test_auc = roc_auc_score(y_test, y_scores)
	eProbHT_auc = roc_auc_score(y_test, test_eProbHT)
	print("LSTM AUC: ", test_auc)
	print("eProbHT AUC: ", roc_auc_score(y_test, test_eProbHT))
	fpr, tpr, thresholds = roc_curve(y_test, y_scores)
	print("Epoch: {}: Test AUC: {}".format(epoch+1, test_auc))

	plt.figure()
	plotRocCurve(fpr, tpr, label = "LSTM (hits) ({})".format(np.round(test_auc,4)))
	fpr, tpr, thresholds = roc_curve(y_test, test_eProbHT)
	plotRocCurve(fpr,tpr, label = "eProbHT ({})".format(np.round(eProbHT_auc,4)))
	plt.title("Pytorch LSTM Hits - {} epochs".format(epoch + 1))
	plt.legend()
	plt.savefig("ROC_LSTM_Pytorch_epoch{}".format(epoch + 1))
	#plt.show()