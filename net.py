"""Defines the LSTM network, loss function and metrics"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

#define Model Class
class Model(nn.Module):

    def __init__(self,input_size,hidden_size,num_layers,batch_size,num_classes,dropout):
        super(Model, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size 
        self.num_layers = num_layers
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.dropout = dropout
        self.lstm = nn.LSTM(input_size=input_size,
                            hidden_size=hidden_size, num_layers=num_layers, 
                            dropout = dropout, batch_first=True)
        self.hidden2label = nn.Linear(hidden_size, num_classes)
        self.sigmoid = torch.nn.Sigmoid()
        
    def forward(self, x, hidden):

        sequence_length = self.batch_size
    
        x = x.view(self.batch_size, sequence_length, self.input_size)
        # Propagate input through RNN
        # Input: (batch, seq_len, input_size)
        # hidden: (num_layers * num_directions, batch, hidden_size)
        out, hidden = self.lstm(x, hidden)
        class_score = self.hidden2label(out)
        return self.sigmoid(class_score), hidden

    def init_hidden(self):
        # Initialize hidden and cell states
        # (num_layers * num_directions, batch, hidden_size)
        return (torch.randn(self.num_layers, 1, self.hidden_size),
                torch.randn(self.num_layers, 1, self.hidden_size))


