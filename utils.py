"""
Helper functions used during the trainign/testing/evaluation phases
"""

def plotRocCurve(fpr, tpr, label = None):
	"""
	Plots the ROC curve given the False Positive Rate (fpr) and True Positive Rate (tpr) arrays

	Args:
	fpr: Array containing False Positive Rates (returned from sklearn.metrics.roc_auc score)
	tpr: Array containing True Positive Rates (returned from sklearn.metrics.roc_auc score)
	label (optional): Label for the plot
	"""
	from matplotlib import pyplot as plt
	
	plt.plot(fpr, tpr, linewidth = 2, label= label)
	plt.plot([0, 1], [0,1], 'k--')
	plt.axis([0,1,0,1])
	plt.xlabel("False Positive Rate")
	plt.ylabel("True Positive Rate")

def timeSince(since):
	"""
	Calculates the elapsed time from 'since' in human-readible way in minutes and seconds

	Args:
	since: The point of time from which the time elapsed will be calculated (eg. for now: time.time())
	"""
	import time
	import math

	now = time.time()
	s = now - since
	m = math.floor(s / 60)
	s -= m * 60
	return '%dm %ds' % (m, s)

def saveModel(model, optimizer, path, filename, comment = "Saved Model"):
	"""
	Saves the given trained Pytorch model state (parameters etc. )to given path with a given filename
	Args:
	model: Pytorch model used for training
	optimizer: Optimizer used for the training
	path: Full path of the file to be saved
	filename: Name for the file to be saved
	comment = Any comments regarding the saved model
	"""
	import os
	state = { 'comment': comment,
			  'state_dict': model.state_dict(),
			  'optimizer': optimizer.state_dict(),
			  'comment: ': '10 epoch 700k e/500k mu data - Only 6 hits - Adagrad lr = 0.1'}

	fileName = os.path.join(path, filename)
	torch.save(state, filename)

	return 0
