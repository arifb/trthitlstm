"""Prepare the dataset to be used in the training and test
phases of the model using functions: makeData and makeTrainTestdata

If the input variables to be changed, correspondingly the data preparation
functions should also be modified accordingly.
"""


import matplotlib.pyplot as plt
import numpy as np
import uproot
import time
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, roc_auc_score
import glob

def findFiles(path): 
	"""Finds all of the file in a directory whose path is given
	"""
	return glob.glob(path)


def makeData(dict_Zee, dict_Zmumu, part = 1, sample_size = (1000,1000)):
	"""
	Arguments:
	--dict_Zee (dict_Zmumu) (string): path to directory containing Zee (Zmumu) files
	--part (integer): Portion of the data which will be used e_files[:part] & m_files[:part] - given as number of files
	By default it returns the first listed file in the directory
	--sample_size (Tuple) Number of tracks randomly selected from training set respectively from e and mu
	By default it returns 10k tracks both from e and m
	
	Outputs:
	--dataset: Dataframe consisting of sampled electron and muons with a given feature set
	"""
	e_files = findFiles(dict_Zee + '/*.root')
	m_files = findFiles(dict_Zmumu + '/*.root')   
	e_files = e_files[:part]
	m_files = m_files[:part]

	#ELECTRONS
	#Modify and add the corresponding array for added feature to the model as follows:
	#Track  variables 
	e_p = []; e_eta = []; e_eProbHT =[]; e_label = []; e_fHTMB = [];e_trkOcc = []  
	#Hit variables
	e_hit_HTMB = []; e_hit_gasType = []; e_hit_drifttime = []; e_hit_tot = []; e_hit_T0 = []; e_hit_L = []
	e_hit = []
	features = ["p","eta", "eProbHT", "hit_HTMB", "hit_gasType", "hit_drifttime", "hit_tot", "hit_T0", "hit_L"]
	print("Reading the electron data")

	for k in e_files:    
		for arrays in uproot.iterate(k, "electron_mc",features): 
			#print("len pT: ", len(arrays[b'pT']))
			for i in range(len(arrays[b'p'])):
				e_p.append(arrays[b'p'][i])
				e_eta.append(arrays[b'eta'][i])
				e_eProbHT.append(arrays[b'eProbHT'][i])
				track_prob = arrays[b'eProbHT'][i]
				hit = []
				if(arrays[b'hit_HTMB'][i][-1] == 99999):
					for j in range(list(arrays[b'hit_HTMB'][i]).index(99999)):
						hit.append([arrays[b'hit_HTMB'][i][j], arrays[b'hit_gasType'][i][j],
									arrays[b'hit_drifttime'][i][j], arrays[b'hit_tot'][i][j],
									arrays[b'hit_T0'][i][j], arrays[b'hit_L'][i][j]])
				else:
					for j in range(len(arrays[b'hit_HTMB'][i])):
						hit.append([arrays[b'hit_HTMB'][i][j], arrays[b'hit_gasType'][i][j],
									arrays[b'hit_drifttime'][i][j], arrays[b'hit_tot'][i][j],
									arrays[b'hit_T0'][i][j], arrays[b'hit_L'][i][j]])                
				e_hit.append(hit)        
				e_label.append(1) # Label = 1 for electron

	print("Electron data read succesfully!")    

	#MUONS
	#Modify and add the corresponding array for added feature to the model as follows:
	#Track  variables     
	m_p = []; m_eta = []; m_eProbHT = []; m_label = []; m_fHTMB = [];m_trkOcc = [];
	#Hit variables
	m_hit_HTMB = []; m_hit_gasType = []; m_hit_drifttime = []; m_hit_tot = []; m_hit_T0 = []; m_hit_L = [];
	m_hit = []

	print("Reading the muon data")
	for k in m_files:    
		for arrays in uproot.iterate(k, "muon_mc",features): 
			for i in range(len(arrays[b'p'])):
				m_p.append(arrays[b'p'][i])
				m_eta.append(arrays[b'eta'][i])
				m_eProbHT.append(arrays[b'eProbHT'][i])
				track_prob = arrays[b'eProbHT'][i]
				hit = []
				if(arrays[b'hit_HTMB'][i][-1] == 99999):
					for j in range(list(arrays[b'hit_HTMB'][i]).index(99999)):
						hit.append([arrays[b'hit_HTMB'][i][j], arrays[b'hit_gasType'][i][j],
									arrays[b'hit_drifttime'][i][j], arrays[b'hit_tot'][i][j],
									arrays[b'hit_T0'][i][j], arrays[b'hit_L'][i][j]])
				else:
					for j in range(len(arrays[b'hit_HTMB'][i])):
						hit.append([arrays[b'hit_HTMB'][i][j], arrays[b'hit_gasType'][i][j],
									arrays[b'hit_drifttime'][i][j], arrays[b'hit_tot'][i][j],
									arrays[b'hit_T0'][i][j], arrays[b'hit_L'][i][j]])                
				m_hit.append(hit)        
				m_label.append(0)

	print("Muon data read succesfully!")
	
	#Creating a dataframe from whole hits from e' and mu's
	#NOTE: Needs to be modified if other track variables added to the model
	raw_data_e = {
			'eProbHT': e_eProbHT,
			'p': e_p,
			'eta': e_eta,
			'hit': e_hit,
			'label': e_label
	}
	df_e = pd.DataFrame(raw_data_e, columns = ["eProbHT","eta", "p","hit","label"])
	raw_data_m = {
			'eProbHT': m_eProbHT,
			'p': m_p,
			'eta': m_eta,
			'hit': m_hit,
			'label': m_label
	}
	df_m = pd.DataFrame(raw_data_m, columns = ["eProbHT","eta", "p", "hit", "label"])
	print("Done!")
	
	df_e = df_e.sample(sample_size[0])
	df_m = df_m.sample(sample_size[1])
	dataset = pd.concat([df_e, df_m]) 

	return dataset

def makeTrainTestData(dataset, test_size = 0.2):
	"""
	Arguments:
	--dataset (pandas dataframe): Dataframe consisting of electron and muon data
	--test_size (float [0,1]): Portion of data to be used as a test set
	
	Outputs:
	--training_data (list): List containing the training data with labels.
	eg. training_data[0] is the first track, the first element(list) is the hit variables, the second is the label 
	--X_test (list): Test set 
	--y_test (list): Labels for the test set (0 for muon, 1 for electron)
	--train_eProbHT (list): Likelihood prob. for training set
	--test_eProbHT (list): Likelihood prob. for test set
	"""
	selected_features = ["eProbHT","eta", "p", "hit"] #all relevant features for the analysis
	X_train, X_test, y_train, y_test = train_test_split(dataset[selected_features], dataset[['label']], test_size=0.2)
	train_features = ["eProbHT","hit"] #Features to be used in the training
	
	X_train = X_train[train_features].values
	X_test = X_test[train_features].values
	y_train = y_train[['label']].values.reshape((len(y_train),))
	y_test= y_test[['label']].values.reshape((len(y_test),))
	
	#remove e_ProbHT from the data to be fed into model
	test_eProbHT = [X_test[i][0] for i in range(len(X_test))]
	train_eProbHT = [X_train[i][0] for i in range(len(X_train))]
	X_train = [list(X_train[i][1]) for i in range(len(X_train))]
	X_test = [list(X_test[i][1]) for i in range(len(X_test))]
	
	training_data = [[X_train[i], y_train[i]] for i in range(len(X_train))]
	print("Train/Test set ready!")
	
	return training_data, X_test, y_test, test_eProbHT, train_eProbHT


if __name__ == '__main__':
	dict_Zee = "/home/arif/cernbox/ml_trt/uproot/Analysis26/sample_data/Zee"
	dict_Zmumu = "/home/arif/cernbox/ml_trt/uproot/Analysis26/sample_data/Zmumu"
	dataset = makeData(dict_Zee, dict_Zmumu)
	training_data, X_test, y_test, test_eProbHT, train_eProbHT = makeTrainTestData(dataset, test_size = 0.2)

	#NORMALIZE THE DATASET FOR NEURAL NETWORK
	#Calculate Means and Variances of hit variables in training & test
	HTMB = []; gasType = []; drifttime = []; tot = []; t0 = []; hit_l = []
	for i in range(len(training_data)):
		for j in range(len(training_data[i][0])):
			HTMB.append(training_data[i][0][j][0])
			gasType.append(training_data[i][0][j][1])
			drifttime.append(training_data[i][0][j][2])
			tot.append(training_data[i][0][j][3])
			t0.append(training_data[i][0][j][4])
			hit_l.append(training_data[i][0][j][5])

			
	for i in range(len(X_test)):
		for j in range(len(X_test[i])):
			HTMB.append(X_test[i][j][0])
			gasType.append(X_test[i][j][1])
			drifttime.append(X_test[i][j][2])
			tot.append(X_test[i][j][3])
			t0.append(X_test[i][j][4])
			hit_l.append(X_test[i][j][5])

	mean_HTMB = np.mean(HTMB); std_HTMB = np.std(HTMB)
	mean_gasType = np.mean(gasType); std_gasType = np.std(gasType)
	mean_drifttime = np.mean(drifttime); std_drifttime = np.std(drifttime)
	mean_tot = np.mean(tot); std_tot = np.std(tot)
	mean_t0 = np.mean(t0); std_t0 = np.std(t0)
	mean_hit_l = np.mean(hit_l); std_hit_l = np.std(hit_l)
	print("***Data Stats***")
	print("mean_HTMB: {}, std_HTMB: {}".format(mean_HTMB, std_HTMB))
	print("mean_gasType: {}, std_gasType: {}".format(mean_gasType, std_gasType))
	print("mean_drifttime: {}, std_drifttime: {}".format(mean_drifttime, std_drifttime))
	print("mean_tot: {}, std_tot: {}".format(mean_tot, std_tot))
	print("mean_t0: {}, std_t0: {}".format(mean_t0, std_t0))
	print("mean_hit_l: {}, std_hit_l: {}".format(mean_hit_l, std_hit_l))


	#Normalize the hit data

	for i in range(len(training_data)):
		for j in range(len(training_data[i][0])):
			training_data[i][0][j][0] = (training_data[i][0][j][0] - mean_HTMB)/std_HTMB
			training_data[i][0][j][1] = (training_data[i][0][j][1] - mean_gasType)/std_gasType
			training_data[i][0][j][2] = (training_data[i][0][j][2] - mean_drifttime)/std_drifttime
			training_data[i][0][j][3] = (training_data[i][0][j][3] - mean_tot)/std_tot
			training_data[i][0][j][4] = (training_data[i][0][j][4] - mean_t0)/std_t0
			training_data[i][0][j][5] = (training_data[i][0][j][5] - mean_hit_l)/std_hit_l


	for i in range(len(X_test)):
		for j in range(len(X_test[i])):
			X_test[i][j][0] = (X_test[i][j][0] - mean_HTMB)/std_HTMB
			X_test[i][j][1] = (X_test[i][j][1] - mean_gasType)/std_gasType
			X_test[i][j][2] = (X_test[i][j][2] - mean_drifttime)/std_drifttime
			X_test[i][j][3] = (X_test[i][j][3] - mean_tot)/std_tot
			X_test[i][j][4] = (X_test[i][j][4] - mean_t0)/std_t0
			X_test[i][j][5] = (X_test[i][j][5] - mean_hit_l)/std_hit_l