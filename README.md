Pytorch RNN - LSTM Model used to train the low-level, hit-based features of the given track registered by the TRT detector.
The data used to train and test the model is provided in explicit path for preparing and testing the data.

Five seperate files used for:

- make_data.py: Preparing th data used for training and testing (including the proper normalisation)
- net.py: Pytorch LSTM model class definition which is used to train the data
- train.py: Training interface for the module 
- evaluate.py: Module used to evaluate the performance of the model and do plotting
- utils.py: Helper functions for the training and testing phase

